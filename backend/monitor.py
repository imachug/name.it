import subprocess
import time

start = time.time()

while time.time() < start + 60 * 60:
	subprocess.run(["python", "backend/handle.py"])
	time.sleep(5)
